CFLAGS = -g -Wall `pkg-config --cflags --libs gtkmm-3.0` --std=c++11
OUTPUT = gtk3bookmarks.out
SRC = *.cpp
OBJ = *.o
COMPIL = g++

Debug : main.o
	$(COMPIL) $(OBJ) -o $(OUTPUT) $(CFLAGS)
main.o : $(SRC)
	$(COMPIL) -c $(SRC) $(CFLAGS)
clean :
	rm -f *~
	rm -f *.o
run :
	./gtk3bookmarks.out 2> /dev/null

