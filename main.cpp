#include <gtkmm.h>
#include <iostream>

#include "MainWindow.h"

int main(int argc, char **argv)
{
	const std::string user_home = std::getenv("HOME");
	const std::string user_user = std::getenv("USER");
	const std::string GLADE_FILE = user_home + "/." + user_user + "/gtk3bookmarks/gtk3-bookmarks.glade";

	std::cout << GLADE_FILE << std::endl;

	Gtk::Main kit(argc,argv);
	Glib::RefPtr<Gtk::Builder> refBuilder = Gtk::Builder::create_from_file(GLADE_FILE);

	MainWindow *window = 0;
	refBuilder->get_widget_derived("window", window);

	if (argc > 1)
		window->addBookmark(argv[1]);

	kit.run(*window);
	return 0;
}

