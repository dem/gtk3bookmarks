#include <iostream>
#include <fstream>
#include <gdk/gdkkeysyms.h>
#include "MainWindow.h"

MainWindow::MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade) : Gtk::ApplicationWindow(cobject), builder(refGlade)
{
	/* ============== */
	/* initialisation */
	/* ============== */

	builder->get_widget("treeview", m_treeview);

	m_refTreeModel = Gtk::ListStore::create(m_columns);
	m_treeview->set_model(m_refTreeModel);

	/* ================== */
	/* remplissage modèle */
	/* ================== */
	
	readBookmarks();

	(m_treeview->get_column(0))->pack_start(m_columns.m_col_chemin);
	(m_treeview->get_column(1))->pack_start(m_columns.m_col_nom);

	// rend la cellule editable
	Gtk::CellRendererText *renderer = dynamic_cast<Gtk::CellRendererText*>(m_treeview->get_column_cell_renderer(1));
	Glib::PropertyProxy<bool> editable = renderer->property_editable();
	editable.set_value(true);
	(m_treeview->get_column(1))->add_attribute(editable, m_columns.m_col_nom);
		
	/* ======= */
	/* signaux */
	/* ======= */

	// catches double-clic sur colonne chemin
	m_treeview->signal_row_activated().connect(sigc::mem_fun(*this, &MainWindow::onRowActivated));
	// catches key suppr event
	m_treeview->add_events(Gdk::KEY_PRESS_MASK);
	m_treeview->signal_key_press_event().connect(sigc::mem_fun(*this, &MainWindow::onKeyPressEvent), false);
	// catches edit colonne nom
	renderer->signal_edited().connect(sigc::mem_fun(*this, &MainWindow::onCellEdited));

	this->signal_delete_event().connect(sigc::mem_fun(*this, &MainWindow::onQuit));
	Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("add_button"))->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::onAdd));
	Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("remove_button"))->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::onRemove));
	Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("edit_button"))->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::onEdit));
	Glib::RefPtr<Gtk::Button>::cast_dynamic(builder->get_object("reset_button"))->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::onReset));
	
}

bool MainWindow::onKeyPressEvent(GdkEventKey* event)
{
	if (event->keyval != GDK_KEY_Delete)
		return false;

	Glib::RefPtr<Gtk::TreeSelection> selection = m_treeview->get_selection();
	m_refTreeModel->erase(selection->get_selected());
	return true;
}

std::string MainWindow::letUserSelectDirectory()
{
	// lance fenetre sélection dossier
	Gtk::FileChooserDialog dialog("Please choose a folder", Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
	dialog.set_transient_for(*this);

	//Add response buttons the the dialog:
	dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	dialog.add_button("Select", Gtk::RESPONSE_OK);

	int result = dialog.run();
	if (result == Gtk::RESPONSE_OK)
		return dialog.get_filename();
	return "";
}

std::array<std::string, 2> MainWindow::getNewRowData()
{
	std::string dir = letUserSelectDirectory();
	if (dir == "")
		return {"", ""};

	if (dir == "/")
		return {dir, dir};
	std::size_t p = dir.find_last_of('/', std::string::npos);
	return {dir, dir.substr(p+1, dir.length())};
}

void MainWindow::onRowActivated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column)
{
	std::array<std::string, 2> strs = getNewRowData();
	if (strs[0] == "")
		return;
	Gtk::TreeModel::Row row = *(m_refTreeModel->get_iter(path));
	row[m_columns.m_col_chemin] = strs[0];
	row[m_columns.m_col_nom] = strs[1];
}

void MainWindow::onCellEdited(const Glib::ustring& path, const Glib::ustring& new_text)
{
	if (new_text == "")
		return;
	Gtk::TreeModel::Row row = *(m_refTreeModel->get_iter(path));
	row[m_columns.m_col_nom] = new_text;
}

bool MainWindow::onQuit(GdkEventAny* event)
{
	writeBookmarks();
	hide();
	return true;
}

void MainWindow::onAdd()
{
	std::array<std::string, 2> strs = getNewRowData();
	if (strs[0] == "")
		return;
	Gtk::TreeModel::Row row = *(m_refTreeModel->append());
	row[m_columns.m_col_chemin] = strs[0];
	row[m_columns.m_col_nom] = strs[1];
}

void MainWindow::onRemove()
{
	Glib::RefPtr<Gtk::TreeSelection> selection = m_treeview->get_selection();
	m_refTreeModel->erase(selection->get_selected());
}

void MainWindow::onEdit()
{
	Glib::RefPtr<Gtk::TreeSelection> selection = m_treeview->get_selection();
	Gtk::TreeModel::Row row = *(selection->get_selected());

	std::array<std::string, 2> strs = getNewRowData();
	if (strs[0] == "")
		return;
	row[m_columns.m_col_chemin] = strs[0];
	row[m_columns.m_col_nom] = strs[1];
}

void MainWindow::onReset()
{
	m_refTreeModel->clear();
	readBookmarks();
}

// TODO gérer dossiers avec espace !!!

void MainWindow::readBookmarks()
{
	Gtk::TreeModel::Row row;

	// open BOOKMARKS_FILE
	std::string line;
	std::ifstream bfile (BOOKMARKS_FILE);
	if (bfile.is_open())
	{
		while ( getline (bfile,line) )
		{
			line = line.substr(7, line.length());
			std::size_t p = line.find_last_of(' ', std::string::npos);
			row = *(m_refTreeModel->append());
			row[m_columns.m_col_chemin] = line.substr(0, p);
			row[m_columns.m_col_nom] = line.substr(p+1, line.length());
		}
		bfile.close();
	}
	else std::cout << "Erreur: impossible d'ouvrir " << BOOKMARKS_FILE << std::endl;
}

void MainWindow::writeBookmarks()
{
	Gtk::TreeModel::Row row;
	std::ofstream bfile (BOOKMARKS_FILE);
	if (bfile.is_open())
	{
		for (unsigned int i=0; i<m_refTreeModel->children().size(); i++)
		{
			row = *(m_refTreeModel->children()[i]);
			bfile << "file://" << row[m_columns.m_col_chemin] << " " << row[m_columns.m_col_nom] << std::endl;
		}
		bfile.close();
	}
	else std::cout << "Erreur: impossible d'ouvrir " << BOOKMARKS_FILE << std::endl;
}

void MainWindow::addBookmark(std::string new_bookmark)
{
	std::string new_name = "";
	if (new_bookmark == "")
		return;
	if (new_bookmark == "/")
		new_name = "/";
	else
	{
		std::size_t p = new_bookmark.find_last_of('/', std::string::npos);
		new_name = new_bookmark.substr(p+1, new_bookmark.length());
	}
	Gtk::TreeModel::Row row = *(m_refTreeModel->append());
	row[m_columns.m_col_chemin] = new_bookmark;
	row[m_columns.m_col_nom] = new_name;
}
