/*
 * MainWindow.h
 *
 *  Created on: November 06, 2014
 *      Author: demeter
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <gtkmm.h>
#include <array>
#include <cstdlib>

class MainWindow : public Gtk::ApplicationWindow
{
protected:
	Glib::RefPtr<Gtk::Builder> builder;

	/////////////////////////////////////////////////////////
	//Tree model columns:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{add(m_col_chemin); add(m_col_nom);}

		Gtk::TreeModelColumn<Glib::ustring> m_col_chemin;
		Gtk::TreeModelColumn<Glib::ustring> m_col_nom;
	};
	ModelColumns m_columns;
	/////////////////////////////////////////////////////////

	Gtk::TreeView *m_treeview;
	Glib::RefPtr<Gtk::ListStore> m_refTreeModel;

private:
	const std::string user_home = std::getenv("HOME"); // getenv return NULL si pas trouvé

public:
	const std::string BOOKMARKS_FILE = user_home + "/.config/gtk-3.0/bookmarks";
	//constructor
	MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
	std::string letUserSelectDirectory();
	std::array<std::string, 2> getNewRowData();

	void readBookmarks();
	void writeBookmarks();

	void addBookmark(std::string new_bookmark);

	// callbacks
	bool onQuit(GdkEventAny* event);
	void onAdd();
	void onRemove();
	void onEdit();
	void onReset();
	void onCellEdited(const Glib::ustring& path, const Glib::ustring& new_text);
	void onRowActivated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
	bool onKeyPressEvent(GdkEventKey* event);
};

#endif
